<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_card', function (Blueprint $table) {
            $table->increments('id');
            $table->char("title",255)->unique()->charset("utf8");
            $table->mediumText("about_user")->charset("utf8");
            $table->char("sity",255)->nullable()->charset("utf8");
            $table->char("phone",255)->nullable()->charset("utf8");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_card');
    }
}