<?php
/**
 * Created by PhpStorm.
 * User: grey
 * Date: 20.07.2018
 * Time: 13:02
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ucard extends Model
{
    protected $fillable = [
        'title','about','sity','phone'
    ];
}