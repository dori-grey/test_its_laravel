<?php
/**
 * Created by PhpStorm.
 * User: grey
 * Date: 20.07.2018
 * Time: 13:08
 */

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UcardResource extends JsonResource
{
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}