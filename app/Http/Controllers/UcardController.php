<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ucard;
use App\Http\Resources\UcardResource;

class UcardController extends Controller
{
    public $validate_rules = [
        'title' => 'required|unique:user_card|max:255',
        'about' => 'required',
        'sity' => 'required|max:255',
        'phone' => 'required|max:255',
    ];

    public function index()
    {
        return UcardResource::collection(Ucard::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate($this->validate_rules);
        try{
            $ucard = Ucard::create([
                'title' => $request->title,
                'about' => $request->about,
                'sity' => $request->sity,
                'phone' => $request->phone,
            ]);
        }catch (\Exception $e){
            return "Error";
        }

        return new UcardResource($ucard);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ucard $ucard)
    {
        return new UcardResource($ucard);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ucard $ucard)
    {
        $own_validate_rules = $this->validate_rules;
        $own_validate_rules['title'] = 'required|max:255|unique:user_card,id,'.$request->id;
        $validatedData = $request->validate($own_validate_rules);
        try {
            $ucard->update($request->all());
        } catch (\Exception $e) {
            return "Error";
        }
        return new UcardResource($ucard);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ucard $ucard)
    {
        $ucard->delete();
        return response()->json(null,204);
    }
}
